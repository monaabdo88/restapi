<?php

namespace App\Http\Controllers\Buyers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Buyer;
class BuyerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $buyers = Buyer::has('transactions')->get();
        return $this->showAll($buyers);
    }
    public function show(Buyer $buyer)
    {
        //$buyer = Buyer::has('transactions')->findOrFail($id);
        return $this->showOne($buyer);
    }
}
