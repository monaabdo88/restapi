<?php

namespace App\Http\Controllers\Buyers;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\Buyer;
class BuyerSellerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(Buyer $buyer)
    {
        $sellers = $buyer->transactions()->with('product.seller')
        ->get()
        ->pluck('product.seller')
        ->unique('id')
        ->values();
        return $this->showAll($sellers);
    }
}
