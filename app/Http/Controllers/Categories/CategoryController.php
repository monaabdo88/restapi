<?php

namespace App\Http\Controllers\Categories;
use App\Http\Controllers\ApiController;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Transformers\CategoryTransformer;
class CategoryController extends ApiController
{
    public function __construct(){
        //parent::__construct();
        $this->middleware('client.credentials')->only(['index','show']);
        $this->middleware('auth:api')->except(['index','show']);
        $this->middleware('transform.input:'.CategoryTransformer::class)->only(['store']);
    }
    public function index()
    {
        $categories = Category::all();
        return $this->showAll($categories);
        
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'          => 'required|min:3',
            'description'   => 'required|min:10'
        ]);
        $newCat = Category::create($request->all());
        return $this->showOne($newCat,201);
    }
    public function show(Category $category)
    {
        return $this->showOne($category);
    }   
    public function update(Request $request, Category $category)
    {
        $category->fill($request->only([
            'name',
            'description'
        ]));
        if($category->isClean()){
            return $this->errorResponse('Please change the value to update the data',422);
        }
        $category->save();
        return $this->showOne($category);

    }
    public function destroy(Category $category)
    {
        $category->delete();
        return $this->showOne($category);
    }
}
