<?php

namespace App\Http\Controllers\Categories;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Category;
class CategoryTransactionController extends ApiController
{   
    public function __construct()
    {
        parent::__construct();
    }
    public function index(Category $category)
    {
        $transactions = $category->products()
        ->whereHas('transactions')
        ->with('transactions')
        ->get()
        ->pluck('transactions')
        ->collapse();
        return $this->showAll($transactions);
    }
}
