<?php

namespace App\Http\Controllers\Products;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
class ProductBuyerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(Product $product)
    {
        $buyers = $product->transactions()->with('buyer')->get()->pluck('buyer')->unique('id')->values();
        return $this->showAll($buyers);
    }
}
