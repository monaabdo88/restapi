<?php

namespace App\Http\Controllers\Products;
use App\Http\Controllers\ApiController;
use App\Models\Product;
use App\Models\User;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Transformers\TransactionTransformer;
class ProductBuyerTransactionController extends ApiController
{
    public function __construct(){
        parent::__construct();
        $this->middleware('transform.input:'.TransactionTransformer::class)->only(['store']);
        $this->middleware('scope:purchase-product')->only('store');
    }
    public function store(Request $request,Product $product,User $buyer)
    {
        $this->validate($request,
        [
            'quantity'      => 'required|min:1|integer'
        ]);
        if($buyer->id == $product->seller_id){
            return $this->errorResponse('The buyer must be diffrent from the seller',409);
        }
        if(!$buyer->isVerified()){
            return $this->errorResponse('The buyer must be verified',409);    
        }
        if(!$product->seller->isVerified()){
            return $this->errorResponse('The seller must be verified',409);
        }
        if(!$product->isAvaliable()){
            return $this->errorResponse('This product is not avaliable',409);
        }
        if($product->quantity < $request->quantity){
            return $this->errorResponse('Not engouh unit to do this transaction',409);
        }
        return DB::transaction(function() use($request,$product,$buyer ){
            $product->quantity -= $request->quantity;
            $product->save();
            $transaction = Transaction::create([
                'quantity'      => $request->quantity,
                'product_id'    => $product->id,
                'buyer_id'      => $buyer->id
            ]);
            return $this->showOne($transaction,201);
        });

    }
}
