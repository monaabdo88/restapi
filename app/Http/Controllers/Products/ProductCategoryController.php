<?php

namespace App\Http\Controllers\Products;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
class ProductCategoryController extends ApiController
{
    public function __construct(){
        $this->middleware('client.credentials')->only(['index']);
        $this->middleware('auth:api')->only(['index']);
        $this->middleware('scope:manage-product')->except('index');
    }
    public function index(Product $product)
    {
        $cateogires = $product->categories;
        return $this->showAll($cateogires);
    }
    public function update(Request $request,Product $product,Category $category)
    {
        $product->categories()->syncWithoutDetaching([$category->id]);
        return $this->showAll($product->categories);

    }
    public function destroy(Product $product,Category $category)
    {
        if(! $product->categories()->find($category->id)){
            return $this->errorResponse('This category is not for this product',404);
        }
        $product->categories()->detach($category->id);
        return $this->showAll($product->categories);
    }
}
