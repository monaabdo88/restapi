<?php

namespace App\Http\Controllers\Products;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
class ProductTransactionController extends ApiController
{
    public function __construct(){
        $this->middleware('client.credentials')->only(['index']);
    }
    public function index(Product $product)
    {
        $transactions = $product->transactions;
        return $this->showAll($transactions);
    }
}
