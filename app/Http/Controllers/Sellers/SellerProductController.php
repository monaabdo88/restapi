<?php

namespace App\Http\Controllers\Sellers;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seller;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use App\Transformers\ProductTransformer;
class SellerProductController extends ApiController
{
    public function __construct(){
        parent::__construct();
        $this->middleware('transform.input:'.ProductTransformer::class)->only(['store','update']);
        $this->middleware('scope:manage-product');
    }
    public function index(Seller $seller)
    {
        $products = $seller->products;
        return $this->showAll($products);
    }
    public function store(Request $request,User $seller)
    {
        $this->validate($request,[
            'name'              => 'required|min:3',
            'description'       => 'required|min:3',
            'quantity'          => 'required|integer',
            'image'             => 'required|image'
        ]);
        $data= $request->all();
        $data['image']      = $request->image->store('');
        $data['seller_id']  = $seller->id;
        $data['status'] = Product::UNVALIABLE_PRODUCT;
        $product = Product::create($data);
        return $this->showOne($product);
    }
    public function update(Request $request, Seller $seller,Product $product)
    {
        $this->validate($request,
        [
            'name'          => 'required',
            'description'   => 'required',
            'status'        => 'in:'.Product::UNVALIABLE_PRODUCT.','.Product::AVALIABLE_PRODUCT,
            'image'         => 'image'
        ]);
        $this->checkSeller($seller,$product);
        $product->fill($request->only([
            'name',
            'description',
            'quantity',
            'image'
        ]));
        if($request->has('status')){
            $product->status = $request->status;
            if($product->isAvaliable() && $product->categories()->count() == 0){
                return $this->errorResponse('To make your product active you must have at least one category',409);
            }
        }
        if($request->hasFile('image')){
            Storage::delete($product->image);
            $product->image = $request->image->store('');
        }
        if($product->isClean()){
            return $this->errorResponse('You must change the values to update',422);
        }
        $product->save();
        return $this->showOne($product);


    }
    public function destroy(Seller $seller,Product $product)
    {
        $this->checkSeller($seller,$product);
        Storage::delete($product->image);
        $product->delete();
        return $this->showOne($product);
    }
    protected function checkSeller(Seller $seller , Product $product){
        if($seller->id != $product->seller_id){
            throw new HttpException(422,'The Specified Seller is not the actuel seller of the product');
        }
    }
}
