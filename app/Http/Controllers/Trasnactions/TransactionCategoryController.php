<?php

namespace App\Http\Controllers\Transactions;
use App\Http\Controllers\ApiController;
use App\Models\Transaction;
use Illuminate\Http\Request;
class TransactionCategoryController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(Transaction $transaction)
    {
        $categories = $transaction->product->categories;
        return $this->showAll($categories);
        
    }
}
