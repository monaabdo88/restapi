<?php

namespace App\Http\Controllers\Transactions;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Transaction;
class TransactionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $transactions = Transaction::all();
        return $this->showAll($transactions);
    }
    public function show(Transaction $transaction)
    {
        return $this->showOne($transaction);
    }
}
