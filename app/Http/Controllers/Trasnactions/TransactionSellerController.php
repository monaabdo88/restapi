<?php

namespace App\Http\Controllers\Transactions;
use App\Http\Controllers\ApiController;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionSellerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(Transaction $transaction)
    {
        $sellers = $transaction->product->seller;
        return $this->showOne($sellers);
        
    }
}
