<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\User;
use App\Mail\UserCreated;
use Illuminate\Support\Facades\Mail;
use App\Transformers\UserTransformer;
class UserController extends ApiController
{
    public function __construct(){
        $this->middleware('client.credentials')->only(['store','resend']);
        $this->middleware('auth:api')->except(['store','verifiy','resend']);
        $this->middleware('transform.input:'.UserTransformer::class)->only(['store','update']);
    }
    public function index()
    {
        $users = User::all();
        return $this->showAll($users);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => 'required|min:3',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|confirmed'
        ]);
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['verified'] = User::UNVERIFIED_USER;
        $data['admin'] = User::REGULAR_USER;
        $data['verification_token'] = User::generatVerificationCode();
        $user = User::create($data);
        return $this->showOne($user,201);
    }
    public function show(User $user)
    {
        return $this->showOne($user);
    }
    public function update(Request $request, User $user)
    {
        $this->validate($request,[
            'email'     => 'email|unique:users,email,'.$user->id,
            'password'  => 'confirmed',
            'admin'     => 'in:'.User::ADMIN_USER.','.User::REGULAR_USER
        ]);
        if($request->has('name')){
            $user->name = $request->name;
        }
        if($request->has('email') && $user->email != $request->email){
            $user->verified = User::UNVERIFIED_USER;
            $user->verification_token = User::generatVerificationCode();
            $user->email = $request->email;
        }
        if($request->has('password')){
            $user->password = bcrypt($request->password);
        }
        if($request->has('admin')){
            if(!$user->isVerified()){
                return $this->errorResponse('only verified user can modify the admin field',409);
            }
            $user->admin = $request->admin;
        }
        if(!$user->isDirty()){
            return $this->errorResponse('you should change the values to update data',422);
        }
        $user->save();
        return $this->showOne($user);
    }
    public function destroy(User $user)
    {
        $user->delete();
        return $this->showOne($user);
    }
    public function verifiy($token){
        $user = User::where('verification_token',$token)->firstOrFail();
        $user->verified = User::VERIFIED_USER;
        $user->verification_token = null;
        $user->save();
        return $this->showMsg('This User has been verified Successfully');
    }
    public function resend(User $user){
        if($user->isVerified()){
            return $this->errorResponse('This Email is already Verified',409);
        }

        retry(5,
        function() use ($user){
            Mail::to($user)->send(new UserCreated($user));
        }
            ,100);
        return $this->showMsg('The Verification email has been send');
    }
}
