<?php

namespace App\Http\Middleware;
use Illuminate\Validation\ValidationException;
use Closure;

class TransformInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$transformer)
    {
        $tranformerInput = [];
        foreach($request->request->all() as $input=>$value){
            $tranformerInput[$transformer::originalAttrributes($input)] = $value;
        }
        $request->replace($tranformerInput);
        $response =  $next($request);
        if(isset($response->exception) && $response->exception instanceof ValidationException){
            $data = $response->getData();
            $transformedErrors = [];
            foreach($data->error as $field=>$error){
                $transformField = $transformer::transformAttrributes($field);
                $transformedErrors[$transformField] = str_replace($field,$transformField,$error);
            }
            $data->error = $transformedErrors;
            $response->setData($data);
        }
        return $response;
    }
}
