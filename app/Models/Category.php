<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;
use App\Transformers\CategoryTransformer;
class Category extends Model
{
    use softDeletes;
    protected $dates = ['deleted_at'];
    protected $hidden = ['pivot'];
    protected $fillable = ['name','description'];
    public $transformer = CategoryTransformer::class;
    public function products(){
    
        return $this->belongsToMany('App\Models\Product');   
    
    }
}
