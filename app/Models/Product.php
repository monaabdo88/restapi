<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;
use App\Transformers\ProductTransformer;
class Product extends Model
{
    const AVALIABLE_PRODUCT = "avaliable";
    const UNVALIABLE_PRODUCT = 'unvaliable';
    use softDeletes;
    protected $dates = ['deleted_at'];
    protected $hidden= ['pivot'];
    protected $fillable = ['name','description','quantity','status','image','seller_id'];
    public $transformer = ProductTransformer::class;
    public function isAvaliable(){
        return $this->status == Product::AVALIABLE_PRODUCT;
    }

    public function categories(){

        return $this->belongsToMany('App\Models\Category'); 

    }

    public function seller(){

        return $this->belongsTo('App\Models\Seller');
        
    }
    
    public function transactions(){

        return $this->hasMany('App\Models\Transaction');
        
    }
}
