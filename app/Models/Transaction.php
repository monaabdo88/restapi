<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;
use App\Transformers\TransactionTransformer;
class Transaction extends Model
{
    use softDeletes;
    protected $dates = ['deleted_at'];
    public $transformer = TransactionTransformer::class;
    protected $fillable = ['buyer_id','product_id','quantity'];

    public function buyer()
    {
        return $this->belongsTo('App\Models\Buyer');
    }
    
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
