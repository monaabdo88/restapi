<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Product;
use App\Mail\UserCreated;
use App\Mail\UserMailUpdated;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::created(function($user){
            retry(5, function() use ($user){
                Mail::to($user)->send(new UserCreated($user));
            },100);
        });
        User::updated(function($user){
            if($user->isDirty('email')){
                retry(5,function () use ($user){
                    Mail::to($user)->send(new UserMailUpdated($user));
                }
                ,100);
            }
        });
        Schema::defaultStringLength(191);
        Product::updated(function ($product){
            if($product->quantity == 0 && $product->isAvaliable()){
                $product->status = Product::UNVALIABLE_PRODUCT;
                $product->save();
            }
        });
    }
}
