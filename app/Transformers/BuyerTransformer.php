<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Buyer;
class BuyerTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Buyer $buyer)
    {
        return [
            'identifier'     => (int)$buyer->id,
            'name'           => (string)$buyer->name,
            'email'          => (string)$buyer->email,
            'isVerified'     => (int)$buyer->verified,
            'CreationDate'   => (string)$buyer->created_at,
            'LastUpdate'     => (string)$buyer->updated_at,
            'deleteDate'     => isset($buyer->deleted_at) ? (string)$buyer->deleted_at : null ,
            'links'          => [
                [
                    'rel'       => 'self',
                    'href'      => route('buyers.show',$buyer->id)
                ],
                [
                    'rel'       => 'buyer.categories',
                    'href'      => route('buyers.categories.index',$buyer->id)
                ],
                [
                    'rel'       => 'buyer.products',
                    'href'      => route('buyers.products.index',$buyer->id)
                ],
                [
                    'rel'       => 'buyer.sellers',
                    'href'      => route('buyers.sellers.index',$buyer->id)
                ],
                [
                    'rel'       => 'buyer.transactions',
                    'href'      => route('buyers.transactions.index',$buyer->id)
                ],
                [
                    'rel'       => 'user',
                    'href'      => route('users.show',$buyer->id)
                ]
            ]
        ];
    }
    public static function originalAttrributes($index){
        $attribute = [
            'identifier'        => 'id',
            'name'              => 'name',
            'email'             => 'email',
            'isVerified'        => 'verified',
            'CreationDate'      => 'created_at',
            'LastUpdate'        => 'updated_at',
            'deleteDate'        => 'deleted_at'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
    public static function transformAttrributes($index){
        $attribute = [
            'identifier'        => 'identifier',
            'name'              => 'title',
            'email'             => 'email',
            'verified'          => 'isVerified',
            'created_at'        => 'createdDate',
            'updated_at'        => 'LastUpdate',
            'deleted_at'        => 'deleteDate'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
}
