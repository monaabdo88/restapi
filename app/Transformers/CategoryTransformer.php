<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Category;
class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'identifier'     => (int)$category->id,
            'title'          => (string)$category->name,
            'details'        => (string)$category->description,
            'CreationDate'   => (string)$category->created_at,
            'LastUpdate'     => (string)$category->updated_at,
            'deleteDate'     => isset($category->deleted_at) ? (string)$category->deleted_at : null,
            'links' => [
                    [
                        'rel'       => 'self',
                        'href'      => route('categories.show',$category->id)
                    ],
                    [
                        'rel'       => 'categories.buyers',
                        'href'      => route('categories.buyers.index',$category->id)
                    ],
                    [
                        'rel'       => 'categories.products',
                        'href'      => route('categories.products.index',$category->id)
                    ],
                    [
                        'rel'       => 'categories.sellers',
                        'href'      => route('categories.sellers.index',$category->id)
                    ],
                    [
                        'rel'       => 'categories.transactions',
                        'href'      => route('categories.transactions.index',$category->id)
                    ]
                ] 
        ];
       
    }
    public static function originalAttrributes($index){
        $attribute = [
            'identifier'        => 'id',
            'title'             => 'name',
            'details'           => 'description',
            'CreationDate'      => 'created_at',
            'LastUpdate'        => 'updated_at',
            'deleteDate'        => 'deleted_at'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
    public static function transformAttrributes($index){
        $attribute = [
            'identifier'        => 'identifier',
            'name'              => 'title',
            'description'       => 'details',
            'created_at'        => 'CreationDate',
            'updated_at'        => 'LastUpdate',
            'deleted_at'        => 'deleteDate'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
}
