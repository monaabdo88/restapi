<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Seller;
class SellerTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Seller $seller)
    {
        return [
            'identifier'     => (int)$seller->id,
            'name'           => (string)$seller->name,
            'email'          => (string)$seller->email,
            'isVerified'     => (int)$seller->verified,
            'CreationDate'   => (string)$seller->created_at,
            'LastUpdate'     => (string)$seller->updated_at,
            'deleteDate'     => isset($seller->deleted_at) ? (string)$seller->deleted_at : null,
            'links'          => [
                [
                    'rel'       => 'self',
                    'href'      => route('sellers.show',$seller->id)
                ],
                [
                    'rel'       => 'sellers.categories',
                    'href'      => route('sellers.categories.index',$seller->id)
                ],
                [
                    'rel'       => 'sellers.products',
                    'href'      => route('sellers.products.index',$seller->id)
                ],
                [
                    'rel'       => 'sellers.buyers',
                    'href'      => route('sellers.buyers.index',$seller->id)
                ],
                [
                    'rel'       => 'sellers.transactions',
                    'href'      => route('sellers.transactions.index',$seller->id)
                ],
                [
                    'rel'       => 'user',
                    'href'      => route('users.show',$seller->id)
                ]
            ]  
        ];
    }
    public static function originalAttrributes($index){
        $attribute = [
            'identifier'     => 'id',
            'name'           => 'name',
            'email'          => 'email',
            'isVerified'     => 'verified',
            'CreationDate'   => 'created_at',
            'LastUpdate'     => 'updated_at',
            'deleteDate'     => 'deleted_at'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
    public static function transformAttrributes($index){
        $attribute = [
            'identifier'        => 'identifier',
            'name'              => 'title',
            'email'             => 'email',
            'verified'          => 'isVerified',
            'created_at'        => 'createdDate',
            'updated_at'        => 'LastUpdate',
            'deleted_at'        => 'deleteDate'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
}
