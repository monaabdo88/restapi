<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Transaction;
class TransactionTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Transaction $transaction)
    {
        return [
            'identifier'     => (int)$transaction->id,
            'quantity'       => (int)$transaction->quantity,
            'buyer_id'       => (int)$transaction->buyer_id,
            'product_id'     => (int)$transaction->product_id,
            'CreationDate'   => (string)$transaction->created_at,
            'LastUpdate'     => (string)$transaction->updated_at,
            'deleteDate'     => isset($transaction->deleted_at) ? (string)$transaction->deleted_at : null,
            'links'          => [
                [
                    'rel'       => 'self',
                    'href'      => route('transactions.show',$transaction->id)
                ],
                [
                    'rel'       => 'transaction.categories',
                    'href'      => route('transactions.categories.index',$transaction->id)
                ],
                [
                    'rel'       => 'transaction.seller',
                    'href'      => route('transactions.sellers.index',$transaction->id)
                ],
                [
                    'rel'       => 'buyer',
                    'href'      => route('buyers.show',$transaction->buyer_id)
                ],
                [
                    'rel'       => 'product',
                    'href'      => route('products.show',$transaction->product_id)
                ]
            ]
        ];
    }
    public static function originalAttrributes(){
        $attribute = [
            'identifier'     => 'id',
            'stock'          => 'quantity',
            'buyer_id'       => 'buyer_id',
            'product_id'     => 'product_id',
            'CreationDate'   => 'created_at',
            'LastUpdate'     => 'updated_at',
            'deleteDate'     => 'deleted_at'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
    public static function transformAttrributes($index){
        $attribute = [
            'identifier'        => 'identifier',
            'quantity'          => 'stock',
            'buyer_id'          => 'buyer_id',
            'product_id'        => 'product_id',
            'created_at'        => 'createdDate',
            'updated_at'        => 'LastUpdate',
            'deleted_at'        => 'deleteDate'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
}
