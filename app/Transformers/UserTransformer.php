<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\User;
class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'identifier'     => (int)$user->id,
            'name'           => (string)$user->name,
            'email'          => (string)$user->email,
            'isVerified'     => (int)$user->verified,
            'isAdmin'        => ($user->admin === true),
            'CreationDate'   => (string)$user->created_at,
            'LastUpdate'     => (string)$user->updated_at,
            'deleteDate'     => isset($user->deleted_at) ? (string)$user->deleted_at : null,
            'links'          => [
                'url'       =>'self',
                'href'      => route('users.index',$user->id)
            ]  
        ];
    }
    public static function originalAttrributes($index){
        $attribute = [
            'identifier'     => 'id',
            'name'           => 'name',
            'email'          => 'email',
            'isVerified'     => 'verified',
            'isAdmin'        => 'admin',
            'CreationDate'   => 'created_at',
            'LastUpdate'     => 'updated_at',
            'deleteDate'     => 'deleted_at'   
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
    public static function transformAttrributes($index){
        $attribute = [
            'identifier'        => 'identifier',
            'name'              => 'name',
            'email'             => 'email',
            'verified'          => 'isVerified',
            'admin'             => 'isAdmin',
            'created_at'        => 'CreationDate',
            'updated_at'        => 'LastUpdate',
            'deleted_at'        => 'deleteDate'  
        ];
        return isset($attribute[$index])? $attribute[$index] : null;
    }
}
