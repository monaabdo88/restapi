<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name'          => $faker->word,
        'description'   => $faker->paragraph(3),
        'quantity'      => $faker->numberBetween(1,10),
        'status'        => $faker->randomElement([Product::UNVALIABLE_PRODUCT,Product::AVALIABLE_PRODUCT]),
        'image'         => $faker->randomElement(['1.jpg','2.jpg','3.jpg','4.jpg','5.jpg','6.jpg','7.jpg','8.jpg','9.jpg','10.jpg','11.jpg','12.jpg','13.jpg','14.jpg','15.jpg']),
        'seller_id'     => User::inRandomOrder()->first()->id,
    ];
});
