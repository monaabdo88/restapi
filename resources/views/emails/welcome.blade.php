@component('mail::message')
  #Hello {{$user->name}}
  Thank you for Create an account. Please Verify your email useing this button below:
  @component('mail::button',['url' => route('verify',$user->verification_token)])
      Verify Account
  @endcomponent
  Thanks <br>
  {{config('app.name')}}
@endcomponent
