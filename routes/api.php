<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Buyer Routes
Route::resource('buyers','Buyers\BuyerController',['only'=>['index','show']]);
Route::resource('buyers.transactions','Buyers\BuyerTransactionController',['only'=>['index']]);
Route::resource('buyers.products','Buyers\BuyerProductController',['only'=>['index']]);
Route::resource('buyers.sellers','Buyers\BuyerSellerController',['only'=>['index']]);
Route::resource('buyers.categories','Buyers\BuyerCategoryController',['only'=>['index']]);
//Categories Routes
Route::resource('categories','Categories\CategoryController',['except'=> ['create','edit']]);
Route::resource('categories.products','Categories\CategoryProductController',['only'=> ['index']]);
Route::resource('categories.sellers','Categories\CategorySellerController',['only'=> ['index']]);
Route::resource('categories.buyers','Categories\CategoryBuyerController',['only'=> ['index']]);
Route::resource('categories.transactions','Categories\CategoryTransactionController',['only'=> ['index']]);
//Products Routes
Route::resource('products','Products\ProductController',['only'=>['index','show']]);
Route::resource('products.transactions','Products\ProductTransactionController',['only'=>['index']]);
Route::resource('products.buyers','Products\ProductBuyerController',['only'=>['index']]);
Route::resource('products.buyers.transactions', 'Products\ProductBuyerTransactionController',['only'=>['store']]);
Route::resource('products.categories','Products\ProductCategoryController',['only'=>['index','update','destroy']]);
//Sellers Routes
Route::resource('sellers','Sellers\SellerController',['only'=>['index','show']]);
Route::resource('sellers.transactions', 'Sellers\SellerTransactionController',['only'=>'index']);
Route::resource('sellers.categories', 'Sellers\SellerCategoryController',['only'=>'index']);
Route::resource('sellers.buyers', 'Sellers\SellerBuyerController',['only'=>'index']);
Route::resource('sellers.products', 'Sellers\SellerProductController',['except'=>['create','edit']]);
//Transactions routes
Route::resource('transactions','Transactions\TransactionController',['only'=>['index','show']]);
Route::resource('transactions.categories','Transactions\TransactionCategoryController',['only'=>['index']]);
Route::resource('transactions.sellers','Transactions\TransactionSellerController',['only'=>['index']]);
//Users Routes
Route::resource('users','Users\UserController',['except'=>['create','edit']]);
Route::name('verify')->get('users/verify/{token}','Users\UserController@verifiy');
Route::name('resend')->get('users/{user}/resend','Users\UserController@resend');
Route::post('oauth/token','\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');